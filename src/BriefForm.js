import React , { Component }from 'react';
class BriefForm extends Component{
    constructor(props) {
        super(props);
        this.state = {
          name: "",
          comment:""
        };
    
        this.handleInputChange = this.handleInputChange.bind(this);
      }
    
      handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'string' ? target.text : target.value;
        const name = target.name;
    
        this.setState({
          [name]: value
        });
      }
    
    
      render() {
        return (
          <form>
            <label>
              Title:
              <input
                name="name"
                type="text"
                text={this.state.name}
                onChange={this.handleInputChange} />
            </label>
            <br />
            <label>
              Comment:
              <input
                name="comment"
                type="text"
                value={this.state.comment}
                onChange={this.handleInputChange} />
            </label>
          
          </form>
        );
      }
    }

export default BriefForm;
